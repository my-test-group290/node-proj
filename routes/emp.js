const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router();

router.get('/', (request, response) => {
    const query = 'select * from emp'
    db.pool.execute(query, (error, emp) => {
        response.send(utils.createResult(error, emp))
    })
})

router.post('/add', (request, response) => {
    const data = request.body;
    const query = 'insert into emp (No,Name,Address) values (?,?,?)'
    db.pool.execute(query, [data.No, data.Name, data.Address], (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

router.get('/search', (request, response) => {
    const No = request.query.No;
    const query = 'select * from emp where No = ?'
    db.pool.execute(query, [No], (error, emp) => {
        response.send(utils.createResult(error, emp))
    })
})

router.put('/update', (request, response) => {
    const No = request.query.No;
    const data = request.body;
    const query = 'update emp set Name = ? where No = ?'
    db.pool.execute(query, [data.Name, No], (error, emp) => {
        response.send(utils.createResult(error, emp))
    })
})

router.delete('/delete', (request, response) => {
    const No = request.query.No
    const query = 'delete from emp where No = ?'
    db.pool.execute(query, [No], (error, emp) => {
        response.send(utils.createResult(error, emp))
    })
})

module.exports = router