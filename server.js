const express = require('express')
const cors = require('cors')
const router = require('./routes/emp')

const app = express();

app.use(cors('*'))
app.use(express.json())

app.use('/emp', router)

app.listen(3000, () => {
    console.log("server started on port 3000");
})